import java.util.Scanner;

public class Primo 
{

  public static void main(String[] args)
	{
	  Scanner input = new Scanner(System.in);
	  System.out.print("Da che numero vuoi iniziare? ");
	  long inizio = input.nextLong(); 
	  System.out.print("A che numero vuoi arrivare?  ");
	  long fine = input.nextLong(); 
	  long t1= System.currentTimeMillis();
	  
      for (long i=inizio; i< fine; i++)	    
		{ boolean trovato=true;
		  for (long k=2; k<= i/2; k++)
			if ( (i % k) == 0 ) 
			  { trovato= false;
			    break;
			  }
		    if(trovato) System.out.println(""+ i);

		}
		
	  long t2= (System.currentTimeMillis() - t1) /1000;
	  System.out.println("Senza Thread - Secondi: "+ t2);
		
	}
}