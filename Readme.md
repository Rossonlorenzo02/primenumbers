# Prime Numbers

1. Si vuole implementare un programma che:
-  legga da tastiera i due estremi di un intervallo di numeri (di tipo long);
- visualizzi tutti i numeri primi compresi nell'intervallo;
- (facoltativo) calcoli il tempo trascorso effettuare l'operazione precedente.

Per raggiungere l'obiettivo:

2. Si scriva in linguaggio java una classe ThreadPrimo con le seguenti caratteristiche:
- la classe erediti la classe Thread (o implementi Runnable);
- implementa un costruttore che prende in ingresso  un numero di tipo long e lo memorizza in una variabile privata interna alla classe;
- implementa il metodo run
     public void run(){...}
     il quale verifica se il numero passato al costruttore è primo,
     e se vero visualizza il numero.

3. Si scriva una classe Main che utilizzando la classe ThreadPrimo implementi 
   le consegne elencate al punto 1).

4. Parte facoltativa. Si calcoli quanti secondi vengono impiegati per le operazioni su descritte. A tal scopo si utilizzi il metodo System.currentTimeMillis(); che ritorna un long contenente i milli secondi trascorsi dal 1/1/1970. 

N.B. Testare il programma con numeri molto grandi, per esempio l'intervallo  tra 1000000000 e 1000000100.

P.s. Si riporta in questa directory a titolo informativo una versione del 
codice in java realizzata con singolo thread.
